<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
    .highcharts-figure, .highcharts-data-table table {
        min-width: 360px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

</style>
<h1>New order</h1>
<div class="order-food">
	<h3>Food order</h3>
	<h3 style="position: relative; bottom: 43px;left: 267px;">08-02-2021</h3>
	<div class="content">
		<h4>Order ID:25 | Token:123456789 | Total: $19.00</h4>
	</div>
</div>
<figure class="highcharts-figure">
	<div id="container" style="position: relative; top: 35px; left: 300px;">
	</div>
</figure>
<script>
	Highcharts.chart('container', {
		xAxis: {
			categories: ['01', '03', '05', '07', '09', '11', '13', '15', '17', '19', '21', '23','25','27','29']
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Food ordered',
			data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6,1.1,1.3,3.3]
		}, {
			name: 'Table booked',
			data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8,10.3, 6.6, 4.8]
		}]
	});
</script>
