<?php
/**
 *
 * Created by Navatech.
 * @project fast-food
 * @author  Phuong
 * @email   notteen[at]gmail.com
 * @date    2/2/2021
 * @time    10:53 AM
 * @property-read
 */

namespace frontend\components;
use Yii;
class Cart
{
	public $cartstore;
	public $getCost = 0;
    public function __construct() {
    	$this->cartstore = Yii::$app->session['cart'];
    	$this->getCost = $this->getCost();
    }
    public function actionIndex(){
    	return $this->render('index');
    }
	public function add($data,$quantity =1)
	{
		if (isset($this->cartstore[$data->id])){
			$this->cartstore[$data->id]->qtt = $this->cartstore[$data->id]->qtt+1;
		}else{
			$this->cartstore[$data->id]=$data;
			$this->cartstore[$data->id]->qtt=$quantity;
			Yii::$app->session['cart'] = $this->cartstore;
		}

	}
	public function remove($data){
    	unset($this->cartstore[$data->id]);
		Yii::$app->session['cart'] = $this->cartstore;
	}

	public function getCost(){

    	if ($this->cartstore){
    		foreach ($this->cartstore as $item){
               $this->getCost += $item->qtt*$item->price;
		    }
	    }
    	return $this->getCost;
	}
	public function getTotoItem(){
    	$total = 0;
    	if ($this->cartstore){
    		foreach ($this->cartstore as $item){
    			$total += $item->qtt;
		    }
	    }
    	return $total;
	}
}
?>

