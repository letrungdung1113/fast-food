<?php
/** @var \frontend\controllers\DetailController $detail */

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
$(".add-to-cart").on("click", function () {
        var cart = $(".shopping-cart");
        var imgtodrag = $(this).parent(".item").find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                "opacity": "0.5",
                    "position": "absolute",
                    "height": "150px",
                    "width": "150px",
                    "z-index": "100"
            })
                .appendTo($("body"))
                .animate({
               "top": cart.offset().top + 10,
                    "left": cart.offset().left + 10,
                    "width": 75,
                    "height": 75
            }, 1000, "easeInOutExpo");
            
            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                "width": 0,
                    "height": 0
            }, function () {
                $(this).detach()
            });
        }
    });')
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Gourmet Traditional Restaurant | News</title>
	<meta charset="utf-8">
	<link rel="icon" href="images/favicon.ico">
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery.js"></script>
	<script src="js/jquery-migrate-1.1.1.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/sForm.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<link rel="stylesheet" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
<div class="main">
	<div class="content">
		<div class="container_12">
			<div class="grid_7">
				<h2 class="head2"></h2>
				<div class="news"> <img src="<?=$detail->image?>" alt="" class="img_inner fleft" style="width: 50%">
					<div class="extra_wrapper" style="height: 200px">
						<div class="col1" style="height: 40px; position: relative; top: -30px;"><h2>Name:<?=$detail->name?></h2></div>
						<div class="col1" style="height: 40px; position: relative; top: -30px;"><h2>Menu's name:LUNCH</h2></div>
						<div class="col1" style="height: 40px; position: relative; top: -30px;"><h2>price:<?=$detail->price?></h2></div>
						<?php echo Html::a('Add  to cart',['cart/add-cart','id'=>$detail->id]);?>
				</div>
				</div>
				<h2>descripstion</h2>
				<div class="news mb0">
					<?=$detail->description?>
				</div>
			</div>
			<div class="grid_3 prefix_2">
				<h2 class="head2">Related food</h2>
				<ul class="list l1">
					<li><a href="#">Vivamus vulputate est</a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="bottom_block">
				<div class="grid_6">
					<h3>Follow Us</h3>
					<div class="socials"> <a href="#"></a> <a href="#"></a> <a href="#"></a> </div>
					<nav>
						<ul>
							<li><a href="index.html">Home</a></li>
							<li><a href="about-us.html">About Us</a></li>
							<li><a href="menu.html">Menu</a></li>
							<li><a href="portfolio.html">Portfolio</a></li>
							<li class="current"><a href="news.html">News</a></li>
							<li><a href="contacts.html">Contacts</a></li>
						</ul>
					</nav>
				</div>
				<div class="grid_6">
					<h3>Email Updates</h3>
					<p class="col1">Join our digital mailing list and get news<br>
						deals and be first to know about events</p>
					<form id="newsletter" action="#">
						<div class="success">Your subscribe request has been sent!</div>
						<label class="email">
							<input type="email" value="Enter e-mail address" >
							<a href="#" class="btn" data-type="submit">subscribe</a> <span class="error">*This is not a valid email address.</span> </label>
					</form>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
</body>
</html>
