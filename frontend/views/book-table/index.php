<!DOCTYPE html>
<html lang="en">
<body>
<div class="main">
	<div class="content page1">
		<div class="container_12">
			<div class="grid_12">
				<div class="bookonlinewrapper">
					<div class="container" id="bookonline">
						<h3 class="wow fadeInUp" data-wow-delay="0.3s"> BOOK ONLINE</h3>
						<form >
							<input type="text" class="name wow zoomIn" placeholder="Your Name" >
							<input type="text" class="email wow zoomIn" placeholder="Your E-MAIL">
							<input type="text" class="from wow zoomIn" placeholder="09-06-2014">
							<input type="text" class="to wow zoomIn" placeholder="09-06-2014">
							<input type="text" class="persons wow zoomIn" placeholder="Number of persons">
							<button class="booknow wow fadeInUp"> BOOK NOW </button>

						</form>

					</div>
				</div>
			</div>
			<div class="bottom_block">
				<div class="grid_6">
					<h3>Follow Us</h3>
					<div class="socials"> <a href="#"></a> <a href="#"></a> <a href="#"></a> </div>
					<nav>
						<ul>
							<li class="current"><a href="index.html">Home</a></li>
							<li><a href="about-us.html">About Us</a></li>
							<li><a href="menu.html">Menu</a></li>
							<li><a href="portfolio.html">Portfolio</a></li>
							<li><a href="news.html">News</a></li>
							<li><a href="contacts.html">Contacts</a></li>
						</ul>
					</nav>
				</div>
				<div class="grid_6">
					<h3>Email Updates</h3>
					<p class="col1">Join our digital mailing list and get news<br>
						deals and be first to know about events</p>
					<form id="newsletter" action="#">
						<div class="success">Your subscribe request has been sent!</div>
						<label class="email">
							<input type="email" value="Enter e-mail address" >
							<a href="#" class="btn" data-type="submit">subscribe</a> <span class="error">*This is not a valid email address.</span> </label>
					</form>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<script src="https://kit.fontawesome.com/c75efb11bf.js" crossorigin="anonymous"></script>
<script>
	var cart = document.getElementById("cart");
	var checkout = document.getElementById("checkout");

</script>
</body>
</html>
