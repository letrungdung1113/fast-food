<?php
/**
 * Created by Navatech.
 * @project fast-food
 * @author  Phuong
 * @email   notteen[at]gmail.com
 * @date    20/1/2021
 * @time    4:50 PM
 */

use frontend\controllers\CartController;
use frontend\widgets\Cart;
use yii\helpers\Url;

?>
<script src="https://kit.fontawesome.com/c75efb11bf.js" crossorigin="anonymous"></script>
<header>
	<div class="container_12">
		<div class="grid_12">
			<h1><a href="index.php"><img src="common/images/logo.png" alt=""></a></h1>
			<div class="menu_block">
				<nav>
					<ul class="sf-menu">
						<li class="current"><a href="index.php">Home</a></li>
						<li><a href="#">FOOD MENU</a></li>
						<li><a href="<?= Url::to(['book-table/index'])?>">BOOK A TABLE</a></li>
						<li><a href="#">ABOUT US</a></li>
						<a href="<?=Url::to('index.php?r=cart')?>"><i class="fas fa-shopping-cart"></i><span><?= /** @var CartController $total */
							$total?></span></a>
					</ul>
				</nav>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</header>
