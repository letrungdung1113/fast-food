<?php

use frontend\controllers\CartController;

/**
 * Created by Navatech.
 * @project Default (Template) Project
 * @author  Phuong
 * @email   notteen[at]gmail.com
 * @date    2/2/2021
 * @time    10:50 AM
 */
/** @var Cart $cartstore */
/* @var $this \yii\web\View */
/* @var CartController $cost */
use frontend\components\Cart;
use yii\helpers\Html;
$this->title = 'cat';

?>
<div class="container">
	<?php
	if ($cartstore) : $n =1; ?>
    <table class="table table-hover">
          <thead>
                 <tr>
	                 <th>STT</th>
	                 <th>Food'name</th>
	                 <th>Price</th>
	                 <th>Quantity</th>
	                 <th>Sub-total</th>
	                 <th></th>
                 </tr>
          </thead>
	      <tbody>
	      <?php foreach ($cartstore as $item) :?>
	             <tr>
		             <td><?php echo $n; ?></td>
		             <td><?php echo $item->name; ?></td>
		             <td><?php echo number_format($item->price) ?>$</td>
		             <td><?php echo $item->qtt; ?></td>
		             <td><?php echo number_format($item->price*$item->qtt) ?>$</td>
		             <td>
                         <?php echo Html::a('X',['cart/remove','id'=>$item->id]);?>
		             </td>
	             </tr>
	      <?php $n++; endforeach;?>
	      </tbody>
	             <tr>
		             <th>Total</th>
		             <th></th>
		             <th></th>
		             <th></th>
		             <th><?php echo number_format($cost)?>$</th>
	             </tr>
    </table>
		<div class="action clearfix">
			<?php echo Html::a('Continue food ordering',['/site'],['class'=>'btn btn -success']); ?>
			<?php echo Html::a('Checkout',['checkout/index'],['class'=>'btn btn -success']); ?>
		</div>
	<?php else:?>
	<div class="alert alert-warning">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h1>Thong bao!</h1>gio hang trong ...<?php echo Html::a('Continue food ordering',['/site'],['class'=>'btn btn -success']); ?>

	</div>
	<?php endif; ?>
</div>
