<?php
/** @var \frontend\controllers\CheckoutController $cartstone */
/** @var \frontend\controllers\CheckoutController $model */
/** @var \frontend\controllers\CheckoutController $cost */
$this->title = "thong tin dat hang";

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<div class="container" style="position: relative;bottom: -34px;">
	<?php if ($cartstone) :$n = 1; ?>
	<?php $form = ActiveForm::begin();?>
	<div class="col-md-5">
		<?php echo $form->field($model,'fullname')->textInput(['placeholder'=>'ho va ten']);?>
		<?php echo $form->field($model,'email')->textInput(['placeholder'=>'email']);?>
		<?php echo $form->field($model,'phone')->textInput(['placeholder'=>'phone']);?>
		<?php echo $form->field($model,'address')->textInput(['placeholder'=>'address']);?>
		<input type="hidden" name="amount" value="<?php echo $cost;?>">
		<button type="submit" class="btn btn-primary pull-right" style="position: relative; right: 141px;" href="index.php">Place an order</button>
<!--		--><?php //echo Html::a('Cancel your order',['/site'],['class'=>'btn btn-danger']); ?>
	</div>
</div>
<?php ActiveForm::end()?>
<?php endif;?>

