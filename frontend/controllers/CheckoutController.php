<?php
/**
 * Created by FesVPN.
 * @project fast-food
 * @author  Pham Hai
 * @email   mitto.hai.7356@gmail.com
 * @date    3/2/2021
 * @time    9:46 PM
 */

namespace frontend\controllers;
use common\models\Invoice;
use frontend\components\Cart;
use Yii;
use yii\web\Controller;
use common\models\Food;
use common\models\InvoiceFood;
class CheckoutController extends Controller {
	function actionIndex(){
		$model = new Invoice();
		$cart = new Cart();
		$cartstone = $cart->cartstore;
		$cost = $cart->getCost;
		if ($model->load(Yii::$app->request->post())){
			$info = new Invoice();
			$post = Yii::$app->request->post()['Invoice'];
			$info->fullname = $post['fullname'];
			$info->address = $post['address'];
			$info->email = $post['email'];
			$info->phone = $post['phone'];
			$info->total = '123';
			$info->status = 'pending';
			$info->token = 'token';
			$info->created_at = time();
			$info->notify = '1';
			if ($info->save()){
				$cart = new Cart();
				$cartstone = $cart->cartstore;
				foreach ($cartstone as $item){
				$invoiceF = new InvoiceFood();
				$invoiceF->invoice_id = $info->id;
				$invoiceF->food_id = $item->id;
				$invoiceF->quantity = $item->qtt;
				if ($invoiceF->save()){
					return $this->redirect('index.php');
				}else{
					echo '<pre>';
					print_r($invoiceF->errors);
					die;
				}
					}
				die;
			}else{
				echo '<pre>';
				print_r($info->errors);
				die;
			}

		}
		return $this->render('index',[
			'model'=>$model,
			'cartstone'=>$cartstone,
			'cost' => $cost,
		]);
	}
}
