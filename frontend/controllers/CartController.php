<?php
/**
 * Created by Navatech.
 * @project fast-food
 * @author  Phuong
 * @email   notteen[at]gmail.com
 * @date    22/1/2021
 * @time    12:15 AM
 */
/** @var Cart $cartstone */
namespace frontend\controllers;
use common\models\Food;
use Yii;
use yii\web\Controller;
use frontend\components\Cart;

class CartController extends Controller {
	function actionIndex(){
		$cart = new Cart();
		$cartstone = $cart->cartstore;
		$cost =$cart->getCost;
		$total = $cart->getTotoItem();
		return $this->render('index',[
			'cartstore'=>$cartstone,
			'cost'=>$cost,
			'total'=>$total

		]);
	}
	public function actionAddCart($id)
	{
		$cart = new Cart();
      $model = Food::findOne(['id'=>$id]);
      $cart->add($model);
      return $this->redirect(['/cart']);
      var_dump(Yii::$app->session['cart']);
	}
	public function actionRemove($id){
       $cart = new Cart();
       $model = Food::findOne(['id'=>$id]);
       $cart->remove($model);
       return $this->redirect(['/cart']);
	}
}
