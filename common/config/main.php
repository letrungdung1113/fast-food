<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'cart' => [
	        'class' => 'yii2mod\cart\Cart',
	        // you can change default storage class as following:
	        'storageClass' => [
		        'class' => 'yii2mod\cart\storage\DatabaseStorage',
		        // you can also override some properties
		        'deleteIfEmpty' => true
	        ]
        ],
    ],
];
