<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property string $fullname
 * @property string $address
 * @property string $email
 * @property int $phone
 * @property string $total
 * @property string $status
 * @property string $token
 * @property int $created_at
 * @property int $notify
 *
 * @property InvoiceFood[] $invoiceFoods
 * @property InvoiceTable[] $invoiceTables
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'address', 'email', 'phone', 'total', 'token', 'created_at', 'notify'], 'required'],
            [['phone', 'created_at', 'notify'], 'integer'],
            [['status'], 'string'],
            [['fullname', 'address', 'email', 'total', 'token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'address' => 'Address',
            'email' => 'Email',
            'phone' => 'Phone',
            'total' => 'Total',
            'status' => 'Status',
            'token' => 'Token',
            'created_at' => 'Created At',
            'notify' => 'Notify',
        ];
    }

    /**
     * Gets query for [[InvoiceFoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFoods()
    {
        return $this->hasMany(InvoiceFood::className(), ['invoice_id' => 'id']);
    }

    /**
     * Gets query for [[InvoiceTables]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTables()
    {
        return $this->hasMany(InvoiceTable::className(), ['invoice_id' => 'id']);
    }
}
